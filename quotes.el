;; This program selects a quote randomly from a list of quotes and prints it out.

;; function fish_greeting
;;	 emacs --script ~/src/tiny-programs/quotes.el 2>&1 | tail -n 1
;; end

(setq quotes '(
	       "A mind is its own place, and in itself can make a heaven of hell, a hell of heaven.--John Milton"
	       "Be impatient with actions, and patient with results.--Naval Ravikant"
	       "Beauty is in the work that you put in; it is complete in and of itself.--Naval Ravikant"
	       "All of humanity's problems stem from man's inability to sit quietly in a room alone.--Blaise Pascal"))

(setq l (length quotes))

;; generate random number
(setq r (random l))

(setq quote (nth r quotes))

(message quote)
